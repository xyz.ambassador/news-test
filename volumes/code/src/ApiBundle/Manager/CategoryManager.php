<?php declare(strict_types = 1);

namespace ApiBundle\Manager;

use ApiBundle\Entity\Category;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use ApiBundle\Repository\CategoryRepository;

/**
 * Class CategoryManager
 * @package ApiBundle\Manager
 */
class CategoryManager
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * @var CategoryRepository|EntityRepository
     */
    private $repository;

    /**
     * CategoryManager constructor.
     * @param EntityManagerInterface   $em
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(
        EntityManagerInterface $em,
        EventDispatcherInterface $dispatcher
    ) {
        $this->em = $em;
        $this->dispatcher = $dispatcher;
        $this->repository = $em->getRepository(Category::class);
    }

    /**
     * @return CategoryRepository
     */
    public function getRepository(): CategoryRepository
    {
        return $this->repository;
    }

    /**
     * @param Category $category
     * @return self
     */
    public function flush(Category $category): self
    {
        $this->em->persist($category);
        $this->em->flush();

        return $this;
    }

    /**
     * @param string $name
     * @return Category
     */
    public function create(string $name): Category
    {
        $post = (new Category())
            ->setName($name)
        ;

        $this->flush($post);

        return $post;
    }
}