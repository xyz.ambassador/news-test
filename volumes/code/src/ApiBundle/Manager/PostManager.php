<?php declare(strict_types = 1);

namespace ApiBundle\Manager;

use ApiBundle\Entity\Category;
use ApiBundle\Entity\Post;
use ApiBundle\Entity\PostData;
use ApiBundle\Repository\PostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class PostManager
 * @package ApiBundle\Manager
 */
class PostManager
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * @var PostRepository|EntityRepository
     */
    private $repository;

    /**
     * PostManager constructor.
     * @param EntityManagerInterface   $em
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(
        EntityManagerInterface $em,
        EventDispatcherInterface $dispatcher
    ) {
        $this->em = $em;
        $this->dispatcher = $dispatcher;
        $this->repository = $em->getRepository(Post::class);
    }

    /**
     * @return PostRepository
     */
    public function getRepository(): PostRepository
    {
        return $this->repository;
    }

    /**
     * @param Post $post
     * @return self
     */
    public function flush(Post $post): self
    {
        $this->em->persist($post);
        $this->em->flush();

        return $this;
    }

    /**
     * @param PostData $data
     * @param Category $category
     * @return Post
     */
    public function create(PostData $data, Category $category): Post
    {
        $post = (new Post())
            ->setTitle($data->getTitle())
            ->setText($data->getText())
            ->setCategory($category)
        ;

        $this->flush($post);

        return $post;
    }

    /**
     * @param PostData $data
     * @param Category $category
     * @return Post
     */
    public function update(PostData $data, Category $category): ?Post
    {
        /** @var Post $post */
        if (!$post = $this->getRepository()->find($data->getId())) {
            return null;
        }

        $post
            ->setTitle($data->getTitle())
            ->setText($data->getText())
            ->setCategory($category)
        ;

        $this->flush($post);

        return $post;
    }

    /**
     * @param Post $post
     * @return self
     */
    public function delete(Post $post): self
    {
        $this->em->remove($post);
        $this->em->flush();

        return $this;
    }
}