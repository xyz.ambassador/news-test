<?php

namespace ApiBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class PostData
 * @package ApiBundle\Entity
 */
class PostData
{
    /**
     * @var int
     *
     * @Assert\Type("integer")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    private $title;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    private $text;

    /**
     * @var int
     *
     * @Assert\NotBlank()
     * @Assert\Type("integer")
     */
    private $categoryId;

    /**
     * Set id.
     *
     * @param int $id
     *
     * @return self
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return self
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * Set text.
     *
     * @param string $text
     *
     * @return self
     */
    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text.
     *
     * @return string
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * Get category id.
     *
     * @return int
     */
    public function getCategoryId(): ?int
    {
        return $this->categoryId;
    }

    /**
     * Set category id.
     *
     * @param int $categoryId
     *
     * @return self
     */
    public function setCategoryId(int $categoryId): ?self
    {
        $this->categoryId = $categoryId;

        return $this;
    }
}
