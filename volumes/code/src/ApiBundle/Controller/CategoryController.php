<?php declare(strict_types = 1);

namespace ApiBundle\Controller;

use ApiBundle\Entity\ApiResponse;
use Symfony\Component\HttpFoundation\Response;
use ApiBundle\Entity\Category;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class CategoryController
 * @package ApiBundle\Controller
 */
class CategoryController extends AbstractController
{
    /**
     * List categories.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Return list categories.",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="success", type="boolean"),
     *         @SWG\Property(property="data", type="array", @SWG\Items(ref=@Model(type=Category::class, groups={"api"}))),
     *     )
     * )
     *
     * @SWG\Tag(name="category")
     *
     * @return Response
     * @throws HttpException
     */
    public function getAction()
    {
        if (!$result = $this->getDoctrine()->getRepository(Category::class)->findAll()) {
            throw new HttpException(
                Response::HTTP_NOT_FOUND,
                'There are no categories exist'
            );
        }

        return $this->response(new ApiResponse($result));
    }
}