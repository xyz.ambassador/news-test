#!/bin/bash
set -e

test -d /var/www/news/var/logs/app || mkdir /var/www/news/var/logs/app

if [ -d "/var/www/news/app/config" ]; then
    cp /usr/local/share/parameters.yml /var/www/news/app/config/parameters.yml
    cp /usr/local/share/.env /var/www/news/.env
    chown -R www-data:www-data /var/www/news
fi

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- php-fpm "$@"
fi

exec "$@"
